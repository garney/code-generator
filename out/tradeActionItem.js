
            <div className={style.propsInput}>
                <span className={style.input}>assetId</span>
                <input data-test="asset-id" 
                    type="number"
                    value={state.tradeAction.assetId}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            assetId: Number(e.target.value)
                        }
                    })} 
                    placeholder="assetId">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>assetName</span>
                <input data-test="asset-name" 
                    value={state.tradeAction.assetName}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            assetName: e.target.value
                        }
                    })} 
                    placeholder="assetName">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>assetTicker</span>
                <input data-test="asset-ticker" 
                    value={state.tradeAction.assetTicker}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            assetTicker: e.target.value
                        }
                    })} 
                    placeholder="assetTicker">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>canSell</span>
                <input data-test="can-sell" 
                    type="checkbox"
                    checked={state.tradeAction.canSell}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            canSell: e.target.checked
                        }
                    })}> 
                    
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>duration</span>
                <input data-test="duration" 
                    value={state.tradeAction.duration}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            duration: e.target.value
                        }
                    })} 
                    placeholder="duration">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>expirationTime</span>
                <input data-test="expiration-time" 
                    type="number"
                    value={state.tradeAction.expirationTime}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            expirationTime: Number(e.target.value)
                        }
                    })} 
                    placeholder="expirationTime">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>investDirection</span>
                <input data-test="invest-direction" 
                    value={state.tradeAction.investDirection}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            investDirection: e.target.value
                        }
                    })} 
                    placeholder="investDirection">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>marketRate</span>
                <input data-test="market-rate" 
                    value={state.tradeAction.marketRate}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            marketRate: e.target.value
                        }
                    })} 
                    placeholder="marketRate">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>marketType</span>
                <input data-test="market-type" 
                    value={state.tradeAction.marketType}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            marketType: e.target.value
                        }
                    })} 
                    placeholder="marketType">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>moneyInvestment</span>
                <input data-test="money-investment" 
                    type="number"
                    value={state.tradeAction.moneyInvestment}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            moneyInvestment: Number(e.target.value)
                        }
                    })} 
                    placeholder="moneyInvestment">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>moneyState</span>
                <input data-test="money-state" 
                    value={state.tradeAction.moneyState}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            moneyState: e.target.value
                        }
                    })} 
                    placeholder="moneyState">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>optionCategory</span>
                <input data-test="option-category" 
                    value={state.tradeAction.optionCategory}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            optionCategory: e.target.value
                        }
                    })} 
                    placeholder="optionCategory">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>optionInstanceId</span>
                <input data-test="option-instance-id" 
                    type="number"
                    value={state.tradeAction.optionInstanceId}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            optionInstanceId: Number(e.target.value)
                        }
                    })} 
                    placeholder="optionInstanceId">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>pipSize</span>
                <input data-test="pip-size" 
                    type="number"
                    value={state.tradeAction.pipSize}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            pipSize: Number(e.target.value)
                        }
                    })} 
                    placeholder="pipSize">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>returnRate</span>
                <input data-test="return-rate" 
                    type="number"
                    value={state.tradeAction.returnRate}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            returnRate: Number(e.target.value)
                        }
                    })} 
                    placeholder="returnRate">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>sellPrice</span>
                <input data-test="sell-price" 
                    type="number"
                    value={state.tradeAction.sellPrice}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            sellPrice: Number(e.target.value)
                        }
                    })} 
                    placeholder="sellPrice">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>sellReturnRate</span>
                <input data-test="sell-return-rate" 
                    type="number"
                    value={state.tradeAction.sellReturnRate}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            sellReturnRate: Number(e.target.value)
                        }
                    })} 
                    placeholder="sellReturnRate">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>status</span>
                <input data-test="status" 
                    value={state.tradeAction.status}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            status: e.target.value
                        }
                    })} 
                    placeholder="status">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>strike</span>
                <input data-test="strike" 
                    value={state.tradeAction.strike}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            strike: e.target.value
                        }
                    })} 
                    placeholder="strike">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>tradeActionId</span>
                <input data-test="trade-action-id" 
                    type="number"
                    value={state.tradeAction.tradeActionId}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            tradeActionId: Number(e.target.value)
                        }
                    })} 
                    placeholder="tradeActionId">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>tradeType</span>
                <input data-test="trade-type" 
                    value={state.tradeAction.tradeType}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            tradeType: e.target.value
                        }
                    })} 
                    placeholder="tradeType">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>traderIncome</span>
                <input data-test="trader-income" 
                    type="number"
                    value={state.tradeAction.traderIncome}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            traderIncome: Number(e.target.value)
                        }
                    })} 
                    placeholder="traderIncome">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>tradingStrike</span>
                <input data-test="trading-strike" 
                    value={state.tradeAction.tradingStrike}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            tradingStrike: e.target.value
                        }
                    })} 
                    placeholder="tradingStrike">
                </input> 
            </div>

            <div className={style.propsInput}>
                <span className={style.input}>tradingTime</span>
                <input data-test="trading-time" 
                    type="number"
                    value={state.tradeAction.tradingTime}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE_TRADE_ACTION_ITEM,
                        payload: {
                            tradingTime: Number(e.target.value)
                        }
                    })} 
                    placeholder="tradingTime">
                </input> 
            </div>