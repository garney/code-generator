#!/usr/bin/env node

const chalk = require('chalk');

const error = chalk.bold.red;
const warning = chalk.keyword('orange');

// Delete the 0 and 1 argument (node and script.js)
let args = process.argv.splice(process.execArgv.length + 2);

let generator = args.shift();

if(generator === 'component' || generator === 'comp' || generator === '--c') {
    generator = 'function-component';
}

const { generate } = require('../index.js');

generate(generator, ...args);
