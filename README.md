# Code generator    

a global npm package to help you create code snippets for front end projects

## 🚀 Getting Started

Using [`npm`]():

```bash
npm i @hanasoft/code-generator -g
```

## ✨ Usage

```bash
gen
```
The above command will present you with options:

* component - responsinve app component
* class-component - this will generate a class component
* function-component - this will generate a class component
* story - assumes src > uiLibrary > components  exists, and will list all the components in the folder to choose from 
* helperClass - will create a helper class under src > helpers 
* helperFunction - will create a helper function under src > helpers 
* usereducer - within the component folder, this will genereate a reducer for your useReducer hook


You can also trigger a genrator directly by typing the generator name: 
```bash
gen helperFunction
```

Created a few shorthand generators:
```bash
gen component
```
will create new function component