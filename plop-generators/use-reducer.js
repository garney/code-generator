
const fs = require('fs');
const kebabCase = require('just-kebab-case');
const Console = require('../src/helpers/log');

module.exports = function ()
{
    const cwd = process.cwd();
    const cwdSplit = cwd.split('/');
    const compName = cwdSplit[cwdSplit.length - 1];
    const compKebab = kebabCase(compName);
    const files = fs.readdirSync(cwd);
    let fileName = null;

    files.forEach(file => {
        const item = file.split('.')[0];
        if(item === 'index' || item === compName || item === compKebab) {
            fileName = item
        }
    });

    return {
        description: 'Create a useReducer hook ( you need to be in the component to directory )',
        prompts: [
            {
                type: 'inoput',
                name: 'name',
                message: 'Component name?',
                default: compName
            },
        ],
        actions: [
            {
                type: 'add',
                path: `${cwd}/{{kebabCase name}}-reducer.js`,
                templateFile: `${__dirname}/../plop-templates/reducer.hbs`,
                skip: (data) => {
                    console.log(data);
                    if (data.name === '' || !data.name ) {
                        return 'Cancelled action'
                    }
                    return false;
                }
            },
        ],
    }
}