const fs = require('fs');
const out = require('../src/helpers/log');
const kebabCase = require('just-kebab-case');
module.exports = function (cwd)
{
    cwd = cwd ||  process.cwd();
    const storyBookMainPath = `${cwd}/.storybook/main.js`;
    const defaultCompPath = 'src/components'
    return {
        description: 'Create a class component',
        prompts: [
            {
                type: 'input',
                name: 'name',
                message: 'component name',
            },
            {
                type: 'input',
                name: 'baseFolder',
                message: 'src path name',
                default: defaultCompPath
            },
        ],
        actions: [
            {
                type: 'add',
                path: `${cwd}/{{baseFolder}}/{{pascalCase name}}/index.tsx`,
                templateFile: `${__dirname}/../plop-templates/class-based-component/component.tsx`
            },
            {
                type: 'add',
                path: `${cwd}/src/components/{{pascalCase name}}/{{pascalCase name}}.test.js`,
                templateFile: `${__dirname}/../plop-templates/class-based-component/component.test.js`
            },
            {
                type: 'add',
                path: `${cwd}/{{baseFolder}}/{{pascalCase name}}/{{pascalCase name}}.module.sass`,
                templateFile: `${__dirname}/../plop-templates/class-based-component/component.module.sass`
            }
        ],
    }
}