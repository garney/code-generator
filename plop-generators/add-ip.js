const fs = require('fs');
const out = require('../src/helpers/log');
const kebabCase = require('just-kebab-case');
const ip = require('ip');
const Console = require('../src/helpers/log');

module.exports = function ()
{
    const cwd = process.cwd();
    const routePath = `${cwd}/src/Routes.jsx`;
    const myIp = ip.address();
    Console.info(`Your ip address is: ${myIp}`)
    return {
        description: 'add my ip to route',
        prompts: [
        ],
        actions: [
            {
                type: 'modify',
                path: routePath,
                pattern: /(checkIsWAFeature\([^\)]*\))/s,
                template: `$1 || window.location.hostname.indexOf('${myIp}') >= 0`,
                skip: (data) => {
                    if (fs.existsSync(routePath)) {
                        const routeFile = fs.readFileSync(routePath, 'utf8');
                        
                        if(routeFile.indexOf(myIp) >= 0 ) {
                            // Console.warning('ip  exists in file');
                            return `${myIp} exists in file`;
                        }
                        return false;
                    } else {
                        // Console.warning('File doest not exist');
                        return `${routePath} doest not exist`;
                    }
                }
            }
        ],
    }
}