const fs = require('fs');
const out = require('../src/helpers/log');
const kebabCase = require('just-kebab-case');
module.exports = function ()
{
    const cwd = process.cwd();
    const storyBookMainPath = `${cwd}/.storybook/main.js`;
    return {
        description: 'Create a Helper class',
        prompts: [
            {
                type: 'input',
                name: 'storyPath',
                message: 'storyPath',
            },
            {
                type: 'input',
                name: 'name',
                message: 'component name',
            },
        ],
        actions: [
            {
                type: 'add',
                path: `${cwd}/{{storyPath}}/{{kebabCase name}}.stories.tsx`,
                templateFile: `${__dirname}/../plop-templates/specs.stories.hbs`,
                force: true
            },
            {
                type: 'modify',
                path: storyBookMainPath,
                pattern: /(stories\s*:\s*\[\s*)([^\]]+)/g,
                template: '$1\'../{{storyPath}}/{{kebabCase name}}.stories.tsx\',\n\t$2',
                skip: (data) => {
                    out.log('checking id a sotry book file exist...')
                    if (fs.existsSync(storyBookMainPath)) {
                        
                        const str = fs.readFileSync(storyBookMainPath, 'utf8');
                        // console.log(str)
                        const pattern = new RegExp(`${kebabCase(data.name)}\.stories\.tsx`);        
                        if(str.match(pattern)) {
                            out.warning('story for this component already exist in the storybook main file');
                            return 'Modify storybook file - story for this component already exist in the storybook main file';
                        }
                        console.log(data);
                        return false;
                    } else {
                        out.warning(`storybook file doesn't exist: ${storyBookMainPath}`);
                        return `Modify storybook file - storybook file doesn't exist: ${storyBookMainPath}`;
                    }
                }
            }
        ],
    }
}