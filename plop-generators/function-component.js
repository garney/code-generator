const fs = require('fs');
const out = require('../src/helpers/log');
const kebabCase = require('just-kebab-case');
module.exports = function (cwd)
{
    cwd = cwd ||  process.cwd();
    const defaultCompPath = 'src/components'
    return {
        description: 'Create a Function Component',
        prompts: [
            {
                type: 'input',
                name: 'name',
                message: 'component name',
            },
            {
                type: 'inboput',
                name: 'reducer',
                message: 'Do you want useReducer? [y/n]',
                default: 'y'
            }
        ],
        actions: [
            {
                type: 'add',
                path: `${cwd}/src/components/{{pascalCase name}}/index.tsx`,
                templateFile: `${__dirname}/../plop-templates/function-component/component-reducer.tsx`,
                skip: (data) => {
                    if(data.reducer.toLowerCase() !== 'y') {
                        return 'reducer skip'
                    }
                }
            },
            {
                type: 'add',
                path: `${cwd}/src/components/{{pascalCase name}}/{{pascalCase name}}.test.js`,
                templateFile: `${__dirname}/../plop-templates/function-component/component.test.js`
            },
            {
                type: 'add',
                path: `${cwd}/src/components/{{pascalCase name}}/index.tsx`,
                templateFile: `${__dirname}/../plop-templates/function-component/component.tsx`,
                skip: (data) => {
                    if(data.reducer.toLowerCase() === 'y') {
                        return 'no reducer skip'
                    }
                }
            },
            {
                type: 'add',
                path: `${cwd}/src/components/{{pascalCase name}}/{{pascalCase name}}.module.sass`,
                templateFile: `${__dirname}/../plop-templates/function-component/component.module.sass`
            }
        ],
    }
}