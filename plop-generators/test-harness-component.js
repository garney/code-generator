const fs = require('fs');
const out = require('../src/helpers/log');
const kebabCase = require('just-kebab-case');


function getProps(path, data) {
    // const expression = new RegExp(`Props\\s*\\=\\s*\\{(.+?)\\}`, 's');
    const expression = /Props(<\w*>)*\s*=\s*\{(.+?)(\}\s)/s;
    // const expression = /Props\s*(\<[\w\d]*\>)?\s*\=\s*\{(.+?)\}/si;
    // const expression = new RegExp(`Props\\s*(\\<[\\w\\d]*\\>)?\\s*\\=\\s*\\{(.+?)\\}`, 's');
    const text = fs.readFileSync(path, 'utf8');
    const match = text.match(expression);
    if(!match) {
        return null;
    }
    let matches = match[2];
    if(matches) {
        matches = matches.replace(/\/\/.*\s+/, '');
        return parseProps(matches);
    } else {
        console.log(matches);
        return null;
    }
}

function parseProps(str) {
    const lines = str.split('\n');
    const props = lines.map(item => {
        const matches = item.match(/([\w\d]*)\s*(\?)?\:\s*(.*),?/);
        if(matches && matches[1] && matches[3]) {
            return {
                prop: matches[1],
                optional: matches[2] !== '?',
                type: matches[3].trim().replace(/,$/, ''),
                raw: item.replace(/,/, '').trim()
            }
        }
        return null;
        
    }).filter(item => item)
    console.log(props);
    props.push({
        prop: 'actionText',
        optional: true,
        type: 'string',
        raw: 'actionText: string'
    })
    return props;
}

function propsToInput(item) {
    if(item.type.match(/\=\>/)) {
        return tabs(4);
    } else if(item.type === 'string' ) {
        return `
            <div className={style.propsInput}>
                <span className={style.input}>${item.prop}</span>
                <input data-test="${kebabCase(item.prop)}" 
                    defaultValue={state.${item.prop}}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE,
                        payload: {
                            ${item.prop}: e.target.value
                        }
                    })} 
                    placeholder="${item.prop}">
                </input> 
            </div>`;
    } else if(item.type === 'number') {
        return `
            <div className={style.propsInput}>
                <span className={style.input}>${item.prop}</span>
                <input data-test="${kebabCase(item.prop)}" 
                    defaultValue={state.${item.prop}}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE,
                        payload: {
                            ${item.prop}: Number(e.target.value)
                        }
                    })} 
                    placeholder="${item.prop}">
                </input> 
            </div>`;
    } else if(item.type === 'boolean') {
        return `
            <div className={style.propsInput}>
                <span className={style.input}>${item.prop}</span>
                <input data-test="${kebabCase(item.prop)}" 
                    type="checkbox"
                    defaultChecked={state.${item.prop}}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE,
                        payload: {
                            ${item.prop}: e.target.checked
                        }
                    })} 
                    placeholder="${item.prop}">
                </input> 
                
            </div>`;
    } else {
        return  `
            <div className={style.propsInput}>
                <span className={style.input}>${item.prop}</span>
                <input data-test="${kebabCase(item.prop)}" 
                    defaultValue={state.${item.prop}}
                    onChange={(e) => dispatch({
                        type: TYPES.UPDATE,
                        payload: {
                            ${item.prop}: e.target.value
                        }
                    })} 
                    placeholder="${item.prop}">
                </input> 
            </div>`;
    }
}

function tabs(num) {
    let tab = '';
    for(var i = 0; i< num; i++) {
        tab += '\t';
    }
    return tab;
}
function spaces(num) {
    let tab = '';
    for(var i = 0; i< num; i++) {
        tab += ' ';
    }
    return tab;
}

module.exports = function (cwd)
{
    cwd = cwd ||  process.cwd();
    const defaultCompPath = 'src/responsiveApp/ui';
    const cypressIntegrationPath = `${cwd}/cypress/integration/responsive`;
    const outputCompPath = 'src/testHarness/components';
    const uiDirectory = `${cwd}/${defaultCompPath}/`;
    const outputDirectory = `${cwd}/${outputCompPath}/`;
    // console.log()
    if (!fs.existsSync(uiDirectory)) {
        return {};
    }
    const folders = fs.readdirSync(uiDirectory).filter(item => fs.lstatSync(`${uiDirectory}${item}`).isDirectory());
    const customAction = (data) => (answers) => {
        const expression = new RegExp(`Props\\s*\\=\\s*\\{(.+?)\\}`, 's');
        const path = `${uiDirectory}${answers.name}/index.tsx`;
        answers.props = getProps(path, answers) || [];
        answers.now = Date.now();
    }
    const addPropsToComponent = (text, data) => {
        // console.log(text);
        let props = data.props.filter((item) => item.prop !== 'actionText').map((item) => {
            if(item.type.match(/=>/)) {
                return `${spaces(16)}${item.prop}={() => {\n` +
                `${spaces(20)}dispatch({\n`+
                `${spaces(24)}type: TYPES.UPDATE,\n${spaces(24)}payload: {\n${spaces(28)}actionText: '${item.prop}'\n${spaces(24)}}\n${spaces(20)}});\n`+
                `${spaces(16)}}}`;
            }
            return `${spaces(16)}${item.prop}={state.${item.prop}}`
        } );
        //console.log(props);
        text = text.replace(/--ADD-PROPS-HERE--/, props.join('\n'));
        props = data.props.filter((item) => item.prop !== 'actionText').map(propsToInput);
        //console.log(props);
        text = text.replace(/--ADD-CONTROLS-HERE--/, props.join('\n'));
        
        // console.log(text);
        return text;
    }
    function getInitialValue(item) {
        if(item.type.match(/\=\>/)) {
            return '() => {}';
        } else if(item.type.match(/['"](\w*)['"]/)) {
            return `'${item.type.match(/['"](\w*)['"]/)[1]}'`;
        } 
        else if(item.type.match(/\[\]/)) {
            return `[]`;
        } else if(item.type == 'boolean') {
            return 'false';
        }
        switch (item.type) {
            case 'string':
                return `''`;
                break;
            case 'number':
                return 0;
                break
            default:
                if(item.prop === 'style') {
                    return '{}';
                }
                return '\'UNKNOWN_TYPE\'';

            
        }
    }
    const addPropsToState = (text, data) => {
        let props = data.props.map((item, index) => {
            return `${spaces(4)}${item.raw}${(index+1)< data.props.length?',':''}`
        } );
        text = text.replace(/--ADD-STATE-TYPE-HERE--/, props.join('\n'));
        props = data.props.map((item, index) => {
            const line = `${spaces(4)}${item.prop}: ${getInitialValue(item)}`;
            return `${line}${(index+1)< data.props.length?',':''}`
        } );
        text = text.replace(/--ADD-DEFAULT-STATE-HERE--/, props.join('\n'));
        return text;
    }

    const addTests = (text, data) => {
        
        let props = data.props.filter((item) => !item.type.match(/=\>/)).filter((item) => item.prop !== 'actionText').map((item, index) => {
            let ret = '';
            ret = `${spaces(4)}it('should accept ${item.prop} as props', () => {\n`;
            ret +=`${spaces(8)}cy.getBySel('${kebabCase(item.prop)}').clear();\n`;
            ret +=`${spaces(4)}});\n`;
            return ret;
        } );
        text = text.replace(/--PROPS_CHECK--/, props.join(''));
        return text;
    }

    const addMenu = (text, data)  => {
        return text;
    }
    const formatJSON = (text, data)  => {
        const list = JSON.parse(text);
       
        return JSON.stringify(list, null, 3);
    }
    return {
        description: 'Create a test harness for a compoennt',
        prompts: [
            {
                type: 'list',
                name: 'name',
                message: 'What Component will you be working on?',
                choices: folders
            }
        ],
        actions: [
            customAction(),
            {
                type: 'add',
                force: true,
                path: `${uiDirectory}/{{name}}/test/index.tsx`,
                transform: addPropsToComponent,
                templateFile: `${__dirname}/../plop-templates/test-harness-component/component.tsx`
            },
            {
                type: 'add',
                force: true,
                path: `${uiDirectory}/{{name}}/test//{{pascalCase name}}.module.sass`,
                templateFile: `${__dirname}/../plop-templates/test-harness-component/component.module.sass`
            },
            {
                type: 'add',
                force: true,
                path: `${uiDirectory}/{{name}}/test/{{pascalCase name}}Reducer.ts`,
                transform: addPropsToState,
                templateFile: `${__dirname}/../plop-templates/test-harness-component/component-reducer.ts`
            },
            {
                type: 'add',
                force: true,
                path: `${cypressIntegrationPath}/{{snakeCase name}}.to-do.spec.js`,
                transform: addTests,
                templateFile: `${__dirname}/../plop-templates/test-harness-component/cypress-integration.js`
            },
            // {
            //     type: 'modify',
            //     pattern: /(\{\/\*\*ADD_MENU_ITEM_HERE\*\/\})/,
            //     // template: '<div className={style.menuItem}><Link to={\'/{{camelCase name}}\'}>{{pascalCase name}}</Link> </div>\n'+spaces(16)+'$1',
            //     template: '<div className={style.menuItem}><Link to={\`${pathSuffix}/{{camelCase name}}\`}>{{pascalCase name}}</Link> </div>\n'+spaces(16)+'$1',
            //     skip: (data) => {
            //         const text = fs.readFileSync(`${outputDirectory}/TestMenu/index.tsx`, 'utf8');
            //         const exp = new RegExp(`${data.name}`);
            //         const match = text.match(exp);
            //         // console.log(exp, match, JSON.stringify(data, null, 3));
            //         return match === null? null : 'LINK already exist';
            //     },
            //     path: `${outputDirectory}/TestMenu/index.tsx`,
            //     transform: addMenu
            // },
            {
                type: 'modify',
                pattern: /\]/,
                // template: '<div className={style.menuItem}><Link to={\'/{{camelCase name}}\'}>{{pascalCase name}}</Link> </div>\n'+spaces(16)+'$1',
                template: ',{"route": "{{camelCase name}}", "label": "{{pascalCase name}}Container", "createdDate": {{now}} }]',
                skip: (data) => {
                    const text = fs.readFileSync(`${outputDirectory}/TestMenu/componentList.json`, 'utf8');
                    const list = JSON.parse(text);
                    const comp = list.find((item) => item.route.toLowerCase() === data.name.toLowerCase())
                    return !comp ? null : 'LINK already exist';
                },
                path: `${outputDirectory}/TestMenu/componentList.json`,
                transform: formatJSON
            },
            {
                type: 'modify',
                pattern: /(\{\/\*\*ADD_ROUTE_HERE\*\/\})/,
                // template: '<Route path="/{{camelCase name}}" component=\{ {{pascalCase name}}Container \} />'+'\n'+spaces(16)+'$1',
                template: '<Route path={\`${pathSuffix}/{{camelCase name}}\`} component=\{ {{pascalCase name}}Container \} />'+'\n'+spaces(16)+'$1',
                skip: (data) => {
                    const text = fs.readFileSync(`${outputDirectory}/TestOutputContainer/index.tsx`, 'utf8');
                    const exp = new RegExp(`\\{\\s*${data.name}Container\\s*\\}`);
                    const match = text.match(exp);
                    // console.log(exp, match, JSON.stringify(data, null, 3));
                    return match === null? null : 'ROUTE already exist';
                },
                path: `${outputDirectory}/TestOutputContainer/index.tsx`,
                transform: addMenu
            },
            {
                type: 'modify',
                pattern: /(\/\*\*ADD_CONTAINER_HERE\*\/)/,
                template: 'import {{pascalCase name}}Container from \'@src/responsiveApp/ui/{{name}}/test\';\n$1',
                skip: (data) => {
                    const text = fs.readFileSync(`${outputDirectory}/TestOutputContainer/index.tsx`, 'utf8');
                    const exp = new RegExp(`import\\s${data.name}Container`);
                    const match = text.match(exp);
                    return match === null? null : 'import statement already exist';
                },
                path: `${outputDirectory}/TestOutputContainer/index.tsx`,
                transform: addMenu
            }
        ],
    }
}