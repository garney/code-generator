"use strict";

var fs = require('fs');

var out = require('../src/helpers/log');

var kebabCase = require('just-kebab-case');

module.exports = function (cwd) {
  cwd = cwd || process.cwd();
  var splitCwd = cwd.split('/');
  var folderName = splitCwd[splitCwd.length - 1];
  var defaultCompPath = 'src/components';
  return {
    description: 'Create a JS Librrary',
    prompts: [{
      type: 'input',
      name: 'name',
      message: 'library name',
      "default": folderName
    }, {
      type: 'input',
      name: 'description',
      message: 'describe the library'
    }],
    actions: [{
      type: 'add',
      path: "".concat(cwd, "/src/index.ts"),
      templateFile: "".concat(__dirname, "/../plop-templates/roll-up-js-lib/src/index.hbs")
    }, {
      type: 'add',
      path: "".concat(cwd, "/.gitignore"),
      templateFile: "".concat(__dirname, "/../plop-templates/roll-up-js-lib/.gitignore")
    }, {
      type: 'add',
      path: "".concat(cwd, "/package.json"),
      templateFile: "".concat(__dirname, "/../plop-templates/roll-up-js-lib/package.hbs")
    }, {
      type: 'add',
      path: "".concat(cwd, "/rollup.config.js"),
      templateFile: "".concat(__dirname, "/../plop-templates/roll-up-js-lib/rollup.config.js")
    }, {
      type: 'add',
      path: "".concat(cwd, "/tsconfig.json"),
      templateFile: "".concat(__dirname, "/../plop-templates/roll-up-js-lib/tsconfig.json")
    }]
  };
};