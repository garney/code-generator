"use strict";

var fs = require('fs');

var out = require('../src/helpers/log');

var kebabCase = require('just-kebab-case');

function getProps(path, data) {
  // const expression = new RegExp(`Props\\s*\\=\\s*\\{(.+?)\\}`, 's');
  var expression = /Props(<[0-9A-Z_a-z]*>)*[\t-\r \xA0\u1680\u2000-\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF]*=[\t-\r \xA0\u1680\u2000-\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF]*\{([\s\S]+?)(\}[\t-\r \xA0\u1680\u2000-\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF])/; // const expression = /Props\s*(\<[\w\d]*\>)?\s*\=\s*\{(.+?)\}/si;
  // const expression = new RegExp(`Props\\s*(\\<[\\w\\d]*\\>)?\\s*\\=\\s*\\{(.+?)\\}`, 's');

  var text = fs.readFileSync(path, 'utf8');
  var match = text.match(expression);

  if (!match) {
    return null;
  }

  var matches = match[2];

  if (matches) {
    matches = matches.replace(/\/\/.*\s+/, '');
    return parseProps(matches);
  } else {
    console.log(matches);
    return null;
  }
}

function parseProps(str) {
  var lines = str.split('\n');
  var props = lines.map(function (item) {
    var matches = item.match(/([\w\d]*)\s*(\?)?\:\s*(.*),?/);

    if (matches && matches[1] && matches[3]) {
      return {
        prop: matches[1],
        optional: matches[2] !== '?',
        type: matches[3].trim().replace(/,$/, ''),
        raw: item.replace(/,/, '').trim()
      };
    }

    return null;
  }).filter(function (item) {
    return item;
  });
  console.log(props);
  props.push({
    prop: 'actionText',
    optional: true,
    type: 'string',
    raw: 'actionText: string'
  });
  return props;
}

function propsToInput(item) {
  if (item.type.match(/\=\>/)) {
    return tabs(4);
  } else if (item.type === 'string') {
    return "\n            <div className={style.propsInput}>\n                <span className={style.input}>".concat(item.prop, "</span>\n                <input data-test=\"").concat(kebabCase(item.prop), "\" \n                    defaultValue={state.").concat(item.prop, "}\n                    onChange={(e) => dispatch({\n                        type: TYPES.UPDATE,\n                        payload: {\n                            ").concat(item.prop, ": e.target.value\n                        }\n                    })} \n                    placeholder=\"").concat(item.prop, "\">\n                </input> \n            </div>");
  } else if (item.type === 'number') {
    return "\n            <div className={style.propsInput}>\n                <span className={style.input}>".concat(item.prop, "</span>\n                <input data-test=\"").concat(kebabCase(item.prop), "\" \n                    defaultValue={state.").concat(item.prop, "}\n                    onChange={(e) => dispatch({\n                        type: TYPES.UPDATE,\n                        payload: {\n                            ").concat(item.prop, ": Number(e.target.value)\n                        }\n                    })} \n                    placeholder=\"").concat(item.prop, "\">\n                </input> \n            </div>");
  } else if (item.type === 'boolean') {
    return "\n            <div className={style.propsInput}>\n                <span className={style.input}>".concat(item.prop, "</span>\n                <input data-test=\"").concat(kebabCase(item.prop), "\" \n                    type=\"checkbox\"\n                    defaultChecked={state.").concat(item.prop, "}\n                    onChange={(e) => dispatch({\n                        type: TYPES.UPDATE,\n                        payload: {\n                            ").concat(item.prop, ": e.target.checked\n                        }\n                    })} \n                    placeholder=\"").concat(item.prop, "\">\n                </input> \n                \n            </div>");
  } else {
    return "\n            <div className={style.propsInput}>\n                <span className={style.input}>".concat(item.prop, "</span>\n                <input data-test=\"").concat(kebabCase(item.prop), "\" \n                    defaultValue={state.").concat(item.prop, "}\n                    onChange={(e) => dispatch({\n                        type: TYPES.UPDATE,\n                        payload: {\n                            ").concat(item.prop, ": e.target.value\n                        }\n                    })} \n                    placeholder=\"").concat(item.prop, "\">\n                </input> \n            </div>");
  }
}

function tabs(num) {
  var tab = '';

  for (var i = 0; i < num; i++) {
    tab += '\t';
  }

  return tab;
}

function spaces(num) {
  var tab = '';

  for (var i = 0; i < num; i++) {
    tab += ' ';
  }

  return tab;
}

module.exports = function (cwd) {
  cwd = cwd || process.cwd();
  var defaultCompPath = 'src/responsiveApp/ui';
  var cypressIntegrationPath = "".concat(cwd, "/cypress/integration/responsive");
  var outputCompPath = 'src/testHarness/components';
  var uiDirectory = "".concat(cwd, "/").concat(defaultCompPath, "/");
  var outputDirectory = "".concat(cwd, "/").concat(outputCompPath, "/"); // console.log()

  if (!fs.existsSync(uiDirectory)) {
    return {};
  }

  var folders = fs.readdirSync(uiDirectory).filter(function (item) {
    return fs.lstatSync("".concat(uiDirectory).concat(item)).isDirectory();
  });

  var customAction = function customAction(data) {
    return function (answers) {
      var expression = new RegExp("Props\\s*\\=\\s*\\{(.+?)\\}", 's');
      var path = "".concat(uiDirectory).concat(answers.name, "/index.tsx");
      answers.props = getProps(path, answers) || [];
      answers.now = Date.now();
    };
  };

  var addPropsToComponent = function addPropsToComponent(text, data) {
    // console.log(text);
    var props = data.props.filter(function (item) {
      return item.prop !== 'actionText';
    }).map(function (item) {
      if (item.type.match(/=>/)) {
        return "".concat(spaces(16)).concat(item.prop, "={() => {\n") + "".concat(spaces(20), "dispatch({\n") + "".concat(spaces(24), "type: TYPES.UPDATE,\n").concat(spaces(24), "payload: {\n").concat(spaces(28), "actionText: '").concat(item.prop, "'\n").concat(spaces(24), "}\n").concat(spaces(20), "});\n") + "".concat(spaces(16), "}}");
      }

      return "".concat(spaces(16)).concat(item.prop, "={state.").concat(item.prop, "}");
    }); //console.log(props);

    text = text.replace(/--ADD-PROPS-HERE--/, props.join('\n'));
    props = data.props.filter(function (item) {
      return item.prop !== 'actionText';
    }).map(propsToInput); //console.log(props);

    text = text.replace(/--ADD-CONTROLS-HERE--/, props.join('\n')); // console.log(text);

    return text;
  };

  function getInitialValue(item) {
    if (item.type.match(/\=\>/)) {
      return '() => {}';
    } else if (item.type.match(/['"](\w*)['"]/)) {
      return "'".concat(item.type.match(/['"](\w*)['"]/)[1], "'");
    } else if (item.type.match(/\[\]/)) {
      return "[]";
    } else if (item.type == 'boolean') {
      return 'false';
    }

    switch (item.type) {
      case 'string':
        return "''";
        break;

      case 'number':
        return 0;
        break;

      default:
        if (item.prop === 'style') {
          return '{}';
        }

        return '\'UNKNOWN_TYPE\'';
    }
  }

  var addPropsToState = function addPropsToState(text, data) {
    var props = data.props.map(function (item, index) {
      return "".concat(spaces(4)).concat(item.raw).concat(index + 1 < data.props.length ? ',' : '');
    });
    text = text.replace(/--ADD-STATE-TYPE-HERE--/, props.join('\n'));
    props = data.props.map(function (item, index) {
      var line = "".concat(spaces(4)).concat(item.prop, ": ").concat(getInitialValue(item));
      return "".concat(line).concat(index + 1 < data.props.length ? ',' : '');
    });
    text = text.replace(/--ADD-DEFAULT-STATE-HERE--/, props.join('\n'));
    return text;
  };

  var addTests = function addTests(text, data) {
    var props = data.props.filter(function (item) {
      return !item.type.match(/=\>/);
    }).filter(function (item) {
      return item.prop !== 'actionText';
    }).map(function (item, index) {
      var ret = '';
      ret = "".concat(spaces(4), "it('should accept ").concat(item.prop, " as props', () => {\n");
      ret += "".concat(spaces(8), "cy.getBySel('").concat(kebabCase(item.prop), "').clear();\n");
      ret += "".concat(spaces(4), "});\n");
      return ret;
    });
    text = text.replace(/--PROPS_CHECK--/, props.join(''));
    return text;
  };

  var addMenu = function addMenu(text, data) {
    return text;
  };

  var formatJSON = function formatJSON(text, data) {
    var list = JSON.parse(text);
    return JSON.stringify(list, null, 3);
  };

  return {
    description: 'Create a test harness for a compoennt',
    prompts: [{
      type: 'list',
      name: 'name',
      message: 'What Component will you be working on?',
      choices: folders
    }],
    actions: [customAction(), {
      type: 'add',
      force: true,
      path: "".concat(uiDirectory, "/{{name}}/test/index.tsx"),
      transform: addPropsToComponent,
      templateFile: "".concat(__dirname, "/../plop-templates/test-harness-component/component.tsx")
    }, {
      type: 'add',
      force: true,
      path: "".concat(uiDirectory, "/{{name}}/test//{{pascalCase name}}.module.sass"),
      templateFile: "".concat(__dirname, "/../plop-templates/test-harness-component/component.module.sass")
    }, {
      type: 'add',
      force: true,
      path: "".concat(uiDirectory, "/{{name}}/test/{{pascalCase name}}Reducer.ts"),
      transform: addPropsToState,
      templateFile: "".concat(__dirname, "/../plop-templates/test-harness-component/component-reducer.ts")
    }, {
      type: 'add',
      force: true,
      path: "".concat(cypressIntegrationPath, "/{{snakeCase name}}.to-do.spec.js"),
      transform: addTests,
      templateFile: "".concat(__dirname, "/../plop-templates/test-harness-component/cypress-integration.js")
    }, // {
    //     type: 'modify',
    //     pattern: /(\{\/\*\*ADD_MENU_ITEM_HERE\*\/\})/,
    //     // template: '<div className={style.menuItem}><Link to={\'/{{camelCase name}}\'}>{{pascalCase name}}</Link> </div>\n'+spaces(16)+'$1',
    //     template: '<div className={style.menuItem}><Link to={\`${pathSuffix}/{{camelCase name}}\`}>{{pascalCase name}}</Link> </div>\n'+spaces(16)+'$1',
    //     skip: (data) => {
    //         const text = fs.readFileSync(`${outputDirectory}/TestMenu/index.tsx`, 'utf8');
    //         const exp = new RegExp(`${data.name}`);
    //         const match = text.match(exp);
    //         // console.log(exp, match, JSON.stringify(data, null, 3));
    //         return match === null? null : 'LINK already exist';
    //     },
    //     path: `${outputDirectory}/TestMenu/index.tsx`,
    //     transform: addMenu
    // },
    {
      type: 'modify',
      pattern: /\]/,
      // template: '<div className={style.menuItem}><Link to={\'/{{camelCase name}}\'}>{{pascalCase name}}</Link> </div>\n'+spaces(16)+'$1',
      template: ',{"route": "{{camelCase name}}", "label": "{{pascalCase name}}Container", "createdDate": {{now}} }]',
      skip: function skip(data) {
        var text = fs.readFileSync("".concat(outputDirectory, "/TestMenu/componentList.json"), 'utf8');
        var list = JSON.parse(text);
        var comp = list.find(function (item) {
          return item.route.toLowerCase() === data.name.toLowerCase();
        });
        return !comp ? null : 'LINK already exist';
      },
      path: "".concat(outputDirectory, "/TestMenu/componentList.json"),
      transform: formatJSON
    }, {
      type: 'modify',
      pattern: /(\{\/\*\*ADD_ROUTE_HERE\*\/\})/,
      // template: '<Route path="/{{camelCase name}}" component=\{ {{pascalCase name}}Container \} />'+'\n'+spaces(16)+'$1',
      template: '<Route path={\`${pathSuffix}/{{camelCase name}}\`} component=\{ {{pascalCase name}}Container \} />' + '\n' + spaces(16) + '$1',
      skip: function skip(data) {
        var text = fs.readFileSync("".concat(outputDirectory, "/TestOutputContainer/index.tsx"), 'utf8');
        var exp = new RegExp("\\{\\s*".concat(data.name, "Container\\s*\\}"));
        var match = text.match(exp); // console.log(exp, match, JSON.stringify(data, null, 3));

        return match === null ? null : 'ROUTE already exist';
      },
      path: "".concat(outputDirectory, "/TestOutputContainer/index.tsx"),
      transform: addMenu
    }, {
      type: 'modify',
      pattern: /(\/\*\*ADD_CONTAINER_HERE\*\/)/,
      template: 'import {{pascalCase name}}Container from \'@src/responsiveApp/ui/{{name}}/test\';\n$1',
      skip: function skip(data) {
        var text = fs.readFileSync("".concat(outputDirectory, "/TestOutputContainer/index.tsx"), 'utf8');
        var exp = new RegExp("import\\s".concat(data.name, "Container"));
        var match = text.match(exp);
        return match === null ? null : 'import statement already exist';
      },
      path: "".concat(outputDirectory, "/TestOutputContainer/index.tsx"),
      transform: addMenu
    }]
  };
};