const fs = require('fs');
const out = require('../src/helpers/log');
const kebabCase = require('just-kebab-case');
module.exports = function (cwd)
{
    cwd = cwd ||  process.cwd();
    const defaultCompPath = 'src/responsiveApp/ui'
    return {
        description: 'Create a responsive app component',
        prompts: [
            {
                type: 'input',
                name: 'name',
                message: 'component name',
            },
            {
                type: 'input',
                name: 'baseFolder',
                message: 'src path name',
                default: defaultCompPath
            },
        ],
        actions: [
            {
                type: 'add',
                path: `${cwd}/{{baseFolder}}/{{pascalCase name}}/index.tsx`,
                templateFile: `${__dirname}/../plop-templates/responsive-app-component/component.tsx`
            },
            {
                type: 'add',
                path: `${cwd}/{{baseFolder}}/{{pascalCase name}}/{{pascalCase name}}.module.sass`,
                templateFile: `${__dirname}/../plop-templates/responsive-app-component/component.module.sass`
            }
        ],
    }
}