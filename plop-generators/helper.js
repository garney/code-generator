module.exports = function (helperTemplate)
{
    const cwd = process.cwd();
    return {
        description: 'Create a Helper class',
        prompts: [
            {
                type: 'input',
                name: 'name',
                message: 'Helper class name please',
            },
        ],
        actions: [
            {
                type: 'add',
                path: `${cwd}/src/helpers/{{kebabCase name}}.js`,
                templateFile: `${__dirname}/../plop-templates/${helperTemplate}.hbs`,
            },
        ],
    }
}