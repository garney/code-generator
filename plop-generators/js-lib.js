const fs = require('fs');
const out = require('../src/helpers/log');
const kebabCase = require('just-kebab-case');
module.exports = function (cwd)
{
    cwd = cwd ||  process.cwd();
    const splitCwd = cwd.split('/');
    const folderName = splitCwd[splitCwd.length-1];
    const defaultCompPath = 'src/components'
    return {
        description: 'Create a JS Librrary',
        prompts: [
            {
                type: 'input',
                name: 'name',
                message: 'library name',
                default: folderName
            },
            {
                type: 'input',
                name: 'description',
                message: 'describe the library'
            }
        ],
        actions: [
            {
                type: 'add',
                path: `${cwd}/src/index.ts`,
                templateFile: `${__dirname}/../plop-templates/roll-up-js-lib/src/index.hbs`
            },
            {
                type: 'add',
                path: `${cwd}/.gitignore`,
                templateFile: `${__dirname}/../plop-templates/roll-up-js-lib/.gitignore`
            },
            {
                type: 'add',
                path: `${cwd}/package.json`,
                templateFile: `${__dirname}/../plop-templates/roll-up-js-lib/package.hbs`
            },
            {
                type: 'add',
                path: `${cwd}/rollup.config.js`,
                templateFile: `${__dirname}/../plop-templates/roll-up-js-lib/rollup.config.js`
            },
            {
                type: 'add',
                path: `${cwd}/tsconfig.json`,
                templateFile: `${__dirname}/../plop-templates/roll-up-js-lib/tsconfig.json`
            }
            
        ],
    }
}