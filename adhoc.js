const inquirer = require('inquirer');
const fs = require('fs');
const { getType, propsToInput, getInitialValue } = require('./src/helpers/utils');
(async () => {
    const inFolder = `${__dirname}/input}`;
    const folders = fs.readdirSync(__dirname+'/input').filter(item => !fs.lstatSync(__dirname+'/input/'+item).isDirectory());;
    // console.log(JSON.stringify(folders));
    const dispatchEvent = 'UPDATE';
    const answers = await inquirer
    .prompt([
        {
            type: 'list',
            name: 'name',
            message: 'choose a type to parse?',
            choices: folders
        },
        {
            type: 'input',
            name: 'type',
            message: `dispatch event?`,
            default: dispatchEvent
        },
        {
            type: 'input',
            name: 'prop',
            message: `default prop? (leave blank is not sure)`,
        }
    ]);
    const props = getType(__dirname+'/input/'+answers.name, {});
    const inputs = props.map(item => {
        const text = propsToInput(item, answers.type, answers.prop?`${answers.prop}.`:'');
        return text;
    })
    const initial = props.map((item, index) => {
        const text = getInitialValue(item);
        return `${text}${index<(props.length-1)?',':''}`;
    })
    fs.writeFileSync(__dirname+'/out/'+answers.name, inputs.join('\n'));
    fs.writeFileSync(__dirname+'/out/'+'init_'+answers.name, initial.join('\n'));
    // console.log(props);
    
  
})();
