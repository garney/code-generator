"use strict";

var inquirer = require('inquirer');

var fs = require('fs');

var _require = require('./src/helpers/utils'),
    getType = _require.getType,
    propsToInput = _require.propsToInput,
    getInitialValue = _require.getInitialValue;

(function _callee() {
  var inFolder, folders, dispatchEvent, answers, props, inputs, initial;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          inFolder = "".concat(__dirname, "/input}");
          folders = fs.readdirSync(__dirname + '/input').filter(function (item) {
            return !fs.lstatSync(__dirname + '/input/' + item).isDirectory();
          });
          ; // console.log(JSON.stringify(folders));

          dispatchEvent = 'UPDATE';
          _context.next = 6;
          return regeneratorRuntime.awrap(inquirer.prompt([{
            type: 'list',
            name: 'name',
            message: 'choose a type to parse?',
            choices: folders
          }, {
            type: 'input',
            name: 'type',
            message: "dispatch event?",
            "default": dispatchEvent
          }, {
            type: 'input',
            name: 'prop',
            message: "default prop? (leave blank is not sure)"
          }]));

        case 6:
          answers = _context.sent;
          props = getType(__dirname + '/input/' + answers.name, {});
          inputs = props.map(function (item) {
            var text = propsToInput(item, answers.type, answers.prop ? "".concat(answers.prop, ".") : '');
            return text;
          });
          initial = props.map(function (item, index) {
            var text = getInitialValue(item);
            return "".concat(text).concat(index < props.length - 1 ? ',' : '');
          });
          fs.writeFileSync(__dirname + '/out/' + answers.name, inputs.join('\n'));
          fs.writeFileSync(__dirname + '/out/' + 'init_' + answers.name, initial.join('\n')); // console.log(props);

        case 12:
        case "end":
          return _context.stop();
      }
    }
  });
})();