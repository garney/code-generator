import React, { useState, useEffect, useMemo } from 'react';
import Comp1 from './index';

import * as typography from '@uiLibrary/sass/storybook/typography.module.sass';
import CodeRenderer from '@uiLibrary/components/CodeRenderer';

import var_colors from '!!sass-values-loader!@uiLibrary/sass/colors.sass';

export default {
    component: Comp1,
    title: 'Comp1',
};

const customStyle = {
    color: '#cccccc',
};

const _iconContainerStyle = {
    display: 'flex',
    border: 'solid',
    borderWidth: '1px',
    width: '80px',
    height: '80px',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '8px',
    margin: '8px',
    transform: 'translate(100%, 100%) scale(2)',
};

const options = [
    {
        name: 'Options',
        children: [
            {
                name: 'Basic',
                figmaUrl: 'figma',
            },
            {
                name: 'Basic',
                figmaUrl: 'figma',
            },
            {
                name: 'Basic',
                figmaUrl: 'figma',
            },
        ],
    },
];
const camelToKebab = (str) => str.replace(/[A-Z]/g, (letter) => `-${letter.toLowerCase()}`);

const Usage = (props: {
    data: any;
    name: string;
    color: string;
    warning?: string;
    url?: string;
}) => {
    let warnings = [];

    if (props.warning) {
        if (Array.isArray(props.warning)) {
            warnings = props.warning;
        } else {
            warnings.push(props.warning);
        }
    }
    return (
        <div>
            {warnings.map((warning) => (
                <>
                    <div
                        style={{
                            padding: 16,
                            backgroundColor: var_colors.orange,
                            borderRadius: 8,
                            border: `1px solid ${var_colors.red}`,
                            color: var_colors.white100,
                        }}
                    >
                        Warning: {warning}
                    </div>
                    <br />
                </>
            ))}
            <div
                style={{
                    color: var_colors.yellow,
                    marginBottom: 8,
                }}
                className={typography.body1}
            >
                Javascript usage
            </div>

            <CodeRenderer
                lang={'jsx'}
                code={`
import Comp1 from '@uiLibrary/Comp1;
<Comp1 />

        `}
            />
            <br />
            <div
                style={{
                    color: var_colors.yellow,
                    marginBottom: 8,
                }}
                className={typography.body1}
            >
                Figma Reference
            </div>
            <iframe
                height="400"
                width="400"
                src={`https://www.figma.com/embed?embed_host=lightning&url=${props.url}`}
            />
        </div>
    );
};

const MainView = (props: { selectedItem: any }) => {
    const { selectedItem } = props;
    return (
        <div
            style={{
                marginRight: 32,
                flexGrow: 1,
                flexBasis: 1,
            }}
        >
            <div
                style={{
                    position: 'sticky',
                    top: 0,
                }}
            >
                <div
                    style={{
                        position: 'relative',
                        display: 'inline-flex',
                        alignItems: 'flex-end',
                        marginTop: 16,
                        marginBottom: 24,
                        borderRadius: 8,
                    }}
                >
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexGrow: 0,
                            flexShrink: 0,
                            width: 128,
                            height: 128,
                            borderRadius: 8,
                            marginRight: 16,
                            borderWidth: 1,
                            borderStyle: 'solid',
                            borderColor: var_colors.white100,
                        }}
                    >
                        <Comp1 />
                    </div>
                    <div></div>
                    <div
                        style={{
                            flexGrow: 0,
                            flexShrink: 0,
                            color: var_colors.white100,
                        }}
                        className={typography.heading1}
                    >
                        {selectedItem.name}
                    </div>
                </div>
                <div></div>
                <Usage {...selectedItem} />
            </div>
        </div>
    );
};

export const Comp1Story = () => {
    const [option, setOption] = useState(options[0].children[0]);

    const sideBar = (
        <div
            style={{
                width: 300,
                flexShrink: 0,
                flexGrow: 0,
            }}
        >
            {options.map((group) => (
                <div
                    key={group.name}
                    style={{
                        paddingBottom: 24,
                    }}
                >
                    <div className={typography.sectionHeading}>{group.name}</div>
                    <div style={{}}>
                        {group.children.map((item) => {
                            const property = {};

                            // property[color.propertyName] = var_colors[color.sassName];

                            return (
                                <div
                                    key={item.name}
                                    onClick={() => setOption(item)}
                                    style={{
                                        position: 'relative',
                                        display: 'flex',
                                        marginBottom: 8,
                                        cursor: 'pointer',
                                    }}
                                >
                                    <div
                                        style={{
                                            flexGrow: 0,
                                            flexShrink: 0,
                                            padding: 8,
                                        }}
                                        className={typography.subtitle}
                                    >
                                        {item.name}
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            ))}
        </div>
    );

    return (
        <>
            <div
                style={{
                    display: 'flex',
                }}
            >
                {sideBar}
                <MainView selectedItem={option} />
            </div>
        </>
    );
};
