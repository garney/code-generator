const inquirer = require('inquirer');
const { readdirSync, existsSync } = require('fs');
const { error } = require('./log');

const getDirectories = source =>
  readdirSync(source, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name);

async function storyUtil(path) {

    const UI_LIBRARY_PATH = 'src/uiLibrary/components';
    if(!path) {
        if(existsSync(UI_LIBRARY_PATH)) {
            const dirs = getDirectories(UI_LIBRARY_PATH);
            // console.log(dirs);
            const answers = await inquirer
                .prompt([{
                    type: 'list',
                    name: 'component',
                    message: 'What do you want to generate?',
                    choices: dirs
                }
                ]);
            return {
                storyPath: `${UI_LIBRARY_PATH}/${answers.component}`,
                name: answers.component
            }
        } else {
            error(`unable to find compoennt library folder: ${UI_LIBRARY_PATH}`)
            return null
        }
        
    }
    const pathSplit = path.split('/');
    const name = pathSplit[pathSplit.length - 1];
    return {
        storyPath: path,
        name
    }
    
}
module.exports = storyUtil;