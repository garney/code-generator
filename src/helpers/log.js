const chalk = require('chalk');

const error = (data) =>
{
    console.log(chalk.bold.red(data));
};
const warning = (data) =>
{
    console.log(chalk.keyword('orange')(data));
}
const info = (data) =>
{
    console.log(chalk.keyword('cyan')(data));
};
const log = (data) =>
{
    console.log(chalk.keyword('white')(data));
};

module.exports = {
    error,
    warning,
    info,
    log
};