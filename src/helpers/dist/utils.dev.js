"use strict";

var fs = require('fs');

var kebabCase = require('just-kebab-case');

function getProps(path, data) {
  // const expression = new RegExp(`Props\\s*\\=\\s*\\{(.+?)\\}`, 's');
  var expression = /Props(<[0-9A-Z_a-z]*>)*[\t-\r \xA0\u1680\u2000-\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF]*=[\t-\r \xA0\u1680\u2000-\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF]*\{([\s\S]+?)(\}[\t-\r \xA0\u1680\u2000-\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF])/; // const expression = /Props\s*(\<[\w\d]*\>)?\s*\=\s*\{(.+?)\}/si;
  // const expression = new RegExp(`Props\\s*(\\<[\\w\\d]*\\>)?\\s*\\=\\s*\\{(.+?)\\}`, 's');

  var text = fs.readFileSync(path, 'utf8');
  var match = text.match(expression);

  if (!match) {
    return null;
  }

  var matches = match[2];

  if (matches) {
    matches = matches.replace(/\/\/.*\s+/, '');
    return parseProps(matches);
  } else {
    console.log(matches);
    return null;
  }
}

function getType(path, data) {
  // const expression = new RegExp(`Props\\s*\\=\\s*\\{(.+?)\\}`, 's');
  var expression = /([0-9A-Z_a-z]*)[\t-\r \xA0\u1680\u2000-\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF]*=[\t-\r \xA0\u1680\u2000-\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF]*\{([\s\S]+?)(\}$)/; // const expression = /Props\s*(\<[\w\d]*\>)?\s*\=\s*\{(.+?)\}/si;
  // const expression = new RegExp(`Props\\s*(\\<[\\w\\d]*\\>)?\\s*\\=\\s*\\{(.+?)\\}`, 's');

  var text = fs.readFileSync(path, 'utf8');
  var match = text.trim().match(expression);

  if (!match) {
    return null;
  }

  var matches = match[2];

  if (matches) {
    matches = matches.replace(/\/\/.*\s+/, '');
    return parseProps(matches);
  } else {
    console.log(matches);
    return null;
  }
}

function parseProps(str) {
  var lines = str.split('\n');
  var props = lines.map(function (item) {
    var matches = item.match(/([\w\d]*)\s*(\?)?\:\s*(.*),/); // console.log(matches[2]);

    if (matches && matches[1] && matches[3]) {
      return {
        prop: matches[1],
        optional: matches[2] !== '?',
        type: matches[3],
        raw: item.replace(/,/, '').trim()
      };
    }

    return null;
  }).filter(function (item) {
    return item;
  }); // console.log(props);
  // props.push({
  //     prop: 'actionText',
  //     optional: true,
  //     type: 'string',
  //     raw: 'actionText: string'
  // })

  return props;
}

function propsToInput(item) {
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'UPDATE';
  var defaultProp = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

  if (item.type.match(/\=\>/)) {
    return tabs(4);
  } else if (item.type === 'string') {
    return "\n            <div className={style.propsInput}>\n                <span className={style.input}>".concat(item.prop, "</span>\n                <input data-test=\"").concat(kebabCase(item.prop), "\" \n                    value={state.").concat(defaultProp).concat(item.prop, "}\n                    onChange={(e) => dispatch({\n                        type: TYPES.").concat(action, ",\n                        payload: {\n                            ").concat(item.prop, ": e.target.value\n                        }\n                    })} \n                    placeholder=\"").concat(item.prop, "\">\n                </input> \n            </div>");
  } else if (item.type === 'number') {
    return "\n            <div className={style.propsInput}>\n                <span className={style.input}>".concat(item.prop, "</span>\n                <input data-test=\"").concat(kebabCase(item.prop), "\" \n                    value={state.").concat(defaultProp).concat(item.prop, "}\n                    onChange={(e) => dispatch({\n                        type: TYPES.").concat(action, ",\n                        payload: {\n                            ").concat(item.prop, ": Number(e.target.value)\n                        }\n                    })} \n                    placeholder=\"").concat(item.prop, "\">\n                </input> \n            </div>");
  } else if (item.type.match(/\{/)) {
    return "\n            <div className={style.propsInput}>\n                <span className={style.input}>".concat(item.prop, "</span>\n                <textarea data-test=\"").concat(kebabCase(item.prop), "\" \n                    value={state.").concat(defaultProp).concat(item.prop, "}\n                    onChange={(e) => dispatch({\n                        type: TYPES.").concat(action, ",\n                        payload: {\n                            ").concat(item.prop, ": e.target.value\n                        }\n                    })} \n                    placeholder=\"").concat(item.prop, "\">\n                </textarea> \n            </div>");
  } else if (item.type == 'boolean') {
    return "\n            <div className={style.propsInput}>\n                <span className={style.input}>".concat(item.prop, "</span>\n                <input data-test=\"").concat(kebabCase(item.prop), "\" \n                    type=\"checkbox\"\n                    checked={state.").concat(defaultProp).concat(item.prop, "}\n                    onChange={(e) => dispatch({\n                        type: TYPES.").concat(action, ",\n                        payload: {\n                            ").concat(item.prop, ": e.target.checked\n                        }\n                    })}> \n                    \n                </input> \n            </div>");
  } else {
    return "\n            <div className={style.propsInput}>\n                <span className={style.input}>".concat(item.prop, "</span>\n                <input data-test=\"").concat(kebabCase(item.prop), "\" \n                    value={state.").concat(defaultProp).concat(item.prop, "}\n                    onChange={(e) => dispatch({\n                        type: TYPES.").concat(action, ",\n                        payload: {\n                            ").concat(item.prop, ": e.target.value\n                        }\n                    })} \n                    placeholder=\"").concat(item.prop, "\">\n                </input> \n            </div>");
  }
}

function getInitialValue(item) {
  if (item.type.match(/\=\>/)) {
    return '() => {}';
  } else if (item.type.match(/['"](\w*)['"]/)) {
    return "'".concat(item.type.match(/['"](\w*)['"]/)[1], "'");
  } else if (item.type.match(/\[\]/)) {
    return "".concat(item.prop, ": []");
  } else if (item.type == 'boolean') {
    return "".concat(item.prop, ": false");
  }

  switch (item.type) {
    case 'string':
      return "".concat(item.prop, ": ''");
      break;

    case 'number':
      return "".concat(item.prop, ": 0");
      break;

    default:
      return "".concat(item.prop, ": 'UNKNOWN_TYPE'");
  }
}

function tabs(num) {
  var tab = '';

  for (var i = 0; i < num; i++) {
    tab += '\t';
  }

  return tab;
}

function spaces(num) {
  var tab = '';

  for (var i = 0; i < num; i++) {
    tab += ' ';
  }

  return tab;
}

module.exports = {
  getProps: getProps,
  parseProps: parseProps,
  propsToInput: propsToInput,
  getInitialValue: getInitialValue,
  tabs: tabs,
  spaces: spaces,
  getType: getType
};