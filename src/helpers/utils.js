const fs = require('fs');
const kebabCase = require('just-kebab-case');
function getProps(path, data) {
    // const expression = new RegExp(`Props\\s*\\=\\s*\\{(.+?)\\}`, 's');
    const expression = /Props(<\w*>)*\s*=\s*\{(.+?)(\}\s)/s;
    // const expression = /Props\s*(\<[\w\d]*\>)?\s*\=\s*\{(.+?)\}/si;
    // const expression = new RegExp(`Props\\s*(\\<[\\w\\d]*\\>)?\\s*\\=\\s*\\{(.+?)\\}`, 's');
    const text = fs.readFileSync(path, 'utf8');
    const match = text.match(expression);
    if(!match) {
        return null;
    }
    let matches = match[2];
    if(matches) {
        matches = matches.replace(/\/\/.*\s+/, '');
        return parseProps(matches);
    } else {
        console.log(matches);
        return null;
    }
}

function getType(path, data) {
    // const expression = new RegExp(`Props\\s*\\=\\s*\\{(.+?)\\}`, 's');
    const expression = /(\w*)\s*=\s*\{(.+?)(\}$)/s;
    // const expression = /Props\s*(\<[\w\d]*\>)?\s*\=\s*\{(.+?)\}/si;
    // const expression = new RegExp(`Props\\s*(\\<[\\w\\d]*\\>)?\\s*\\=\\s*\\{(.+?)\\}`, 's');
    const text = fs.readFileSync(path, 'utf8');
    const match = text.trim().match(expression);
    if(!match) {
        return null;
    }
    let matches = match[2];
    if(matches) {
        matches = matches.replace(/\/\/.*\s+/, '');
        return parseProps(matches);
    } else {
        console.log(matches);
        return null;
    }
}

function parseProps(str) {
    const lines = str.split('\n');
    const props = lines.map(item => {
        const matches = item.match(/([\w\d]*)\s*(\?)?\:\s*(.*),/);
        // console.log(matches[2]);
        if(matches && matches[1] && matches[3]) {
            return {
                prop: matches[1],
                optional: matches[2] !== '?',
                type: matches[3],
                raw: item.replace(/,/, '').trim()
            }
        }
        return null;
        
    }).filter(item => item)
    // console.log(props);
    // props.push({
    //     prop: 'actionText',
    //     optional: true,
    //     type: 'string',
    //     raw: 'actionText: string'
    // })
    return props;
}

function propsToInput(item, action = 'UPDATE', defaultProp = '') {
    if(item.type.match(/\=\>/)) {
        return tabs(4);
    } else if(item.type === 'string') {
        return `
            <div className={style.propsInput}>
                <span className={style.input}>${item.prop}</span>
                <input data-test="${kebabCase(item.prop)}" 
                    value={state.${defaultProp}${item.prop}}
                    onChange={(e) => dispatch({
                        type: TYPES.${action},
                        payload: {
                            ${item.prop}: e.target.value
                        }
                    })} 
                    placeholder="${item.prop}">
                </input> 
            </div>`;
    } else if(item.type === 'number') {
        return `
            <div className={style.propsInput}>
                <span className={style.input}>${item.prop}</span>
                <input data-test="${kebabCase(item.prop)}" 
                    value={state.${defaultProp}${item.prop}}
                    onChange={(e) => dispatch({
                        type: TYPES.${action},
                        payload: {
                            ${item.prop}: Number(e.target.value)
                        }
                    })} 
                    placeholder="${item.prop}">
                </input> 
            </div>`;
    } else if(item.type.match(/\{/)) {
        return  `
            <div className={style.propsInput}>
                <span className={style.input}>${item.prop}</span>
                <textarea data-test="${kebabCase(item.prop)}" 
                    value={state.${defaultProp}${item.prop}}
                    onChange={(e) => dispatch({
                        type: TYPES.${action},
                        payload: {
                            ${item.prop}: e.target.value
                        }
                    })} 
                    placeholder="${item.prop}">
                </textarea> 
            </div>`;
    } else if(item.type == 'boolean') {
        return  `
            <div className={style.propsInput}>
                <span className={style.input}>${item.prop}</span>
                <input data-test="${kebabCase(item.prop)}" 
                    type="checkbox"
                    checked={state.${defaultProp}${item.prop}}
                    onChange={(e) => dispatch({
                        type: TYPES.${action},
                        payload: {
                            ${item.prop}: e.target.checked
                        }
                    })}> 
                    
                </input> 
            </div>`;
    } else {
        return  `
            <div className={style.propsInput}>
                <span className={style.input}>${item.prop}</span>
                <input data-test="${kebabCase(item.prop)}" 
                    value={state.${defaultProp}${item.prop}}
                    onChange={(e) => dispatch({
                        type: TYPES.${action},
                        payload: {
                            ${item.prop}: e.target.value
                        }
                    })} 
                    placeholder="${item.prop}">
                </input> 
            </div>`;
    }
}
function getInitialValue(item) {
    if(item.type.match(/\=\>/)) {
        return '() => {}';
    } else if(item.type.match(/['"](\w*)['"]/)) {
        return `'${item.type.match(/['"](\w*)['"]/)[1]}'`;
    } 
    else if(item.type.match(/\[\]/)) {
        return `${item.prop}: []`;
    } else if(item.type == 'boolean') {
        return `${item.prop}: false`;
    }
    switch (item.type) {
        case 'string':
            return `${item.prop}: ''`;
            break;
        case 'number':
            return `${item.prop}: 0`;
            break
        default:
            return `${item.prop}: 'UNKNOWN_TYPE'`;

        
    }
}

function tabs(num) {
    let tab = '';
    for(var i = 0; i< num; i++) {
        tab += '\t';
    }
    return tab;
}
function spaces(num) {
    let tab = '';
    for(var i = 0; i< num; i++) {
        tab += ' ';
    }
    return tab;
}

module.exports = {
    getProps,
    parseProps,
    propsToInput,
    getInitialValue,
    tabs,
    spaces,
    getType
}