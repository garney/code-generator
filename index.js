const nodePlop = require('node-plop');
const chalk = require('chalk');
const prompts = require('prompts');
const inquirer = require('inquirer');
const storyUtil = require('./src/helpers/story-util');

const error = (data) =>
{
    console.log(chalk.bold.red(data));
};
const warning = (data) =>
{
    console.log(chalk.keyword('orange')(data));
}
const info = (data) =>
{
    console.log(chalk.keyword('magenta')(data));
};
// load an instance of plop from a plopfile
const plop = nodePlop(`${__dirname}/plopfile.js`);
// console.logs(plop);

const generatorListToObj = (list) =>
{
    const obj = {};
    list.forEach((item) =>
    {
        obj[item.name] = item
    });
    return obj;
}
const generate = async (name, ...params) =>
{
    const list = plop.getGeneratorList();
    const dictionary = generatorListToObj(list);
    let answers;
    if (dictionary[name]) {
        answers = {
            generator: name
        }
    } else {
        if(!name) {
            warning('choose a generator')
        } else {
            error(`${name} does not exist`);
        }
        // console.log(list);
        answers = await inquirer
            .prompt([{
                type: 'list',
                name: 'generator',
                message: 'What do you want to generate?',
                choices: list.map((item) =>
                {
                    return {
                        name: `${item.name} - ${item.description}`,
                        value: item.name
                    }
                })
            }
            ])
    }
    const { generator } = answers;
    const gen = plop.getGenerator(generator);


    switch(generator) {
        case 'story':
                answers = await storyUtil(...params);
                
                break;
        default:
            answers = await inquirer
                .prompt(gen.prompts);
            break;
    }
    if(answers) {   
        gen.runActions(answers).then(function (results) {
            const { changes, failures} = results;
            if(failures.length > 0) {
                failures.forEach(item => {
                    error(`ERROR: ${item.type} - ${item.error}`);
                })
            } else {
                info('Success!');
                changes.forEach(item => {
                    let str = '';
                    for(let props in item) {
                        str += `${props} - ${item[props]}\n`;
                    }
                    console.log(str);
    
                });
            }
          }).catch((err) => {
                console.log(err);
          });
    }
    
    
};

module.exports = {
    generate
}
