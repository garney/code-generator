const fs = require('fs');
const isResponsiveApp = () => {
    const cwd = process.cwd();
    const defaultCompPath = 'src/responsiveApp/ui';
    const uiDirectory = `${cwd}/${defaultCompPath}/`;
    // console.log(fs.existsSync(uiDirectory));
    return fs.existsSync(uiDirectory);
}

module.exports = function (plop)
{
    plop.setGenerator('component', require('./plop-generators/responsive-app-component')());
    if(isResponsiveApp()) {
        plop.setGenerator('test-harness', require('./plop-generators/test-harness-component')());
    }
    plop.setGenerator('function-component', require('./plop-generators/function-component')());
    // plop.setGenerator('class-component', require('./plop-generators/class-based-component')());
    // plop.setGenerator('story', require('./plop-generators/uiLibrary-story')());
    plop.setGenerator('helperClass', require('./plop-generators/helper')('helper'));
    plop.setGenerator('helperFunction', require('./plop-generators/helper')('helperFunction'));
    // plop.setGenerator('routeIp', require('./plop-generators/add-ip')());
    plop.setGenerator('useReducer', require('./plop-generators/use-reducer')());
    plop.setGenerator('js-library', require('./plop-generators/js-lib')());
    
};
