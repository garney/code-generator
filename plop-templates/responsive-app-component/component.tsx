import React from 'react';

import styles from './{{pascalCase name}}.module.sass';

export type {{pascalCase name}}Props = {
    // TODO: Define your props for {{pascalCase name}}Props
    className?: string,
    style?: any
}

export type {{pascalCase name}}States = {
}

export default class {{pascalCase name}} extends React.PureComponent<{{pascalCase name}}Props, {{pascalCase name}}States> {
    constructor(props: {{pascalCase name}}Props) {
        super(props);
    }

    state = {
    };

    componentDidMount = () => {
        
    };

    componentWillUnmount = () => {
        
    };

    render() {
        return (
            <div className={[
                styles.{{camelCase name}},
                this.props.className
            ].join(' ')} style={this.props.style}>
            </div>
        );
    }
}
