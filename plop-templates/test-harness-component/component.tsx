import React, {useRef, useReducer} from 'react';

import style from './{{pascalCase name}}.module.sass';
import {{pascalCase name}} from '@src/responsiveApp/ui/{{pascalCase name}}';
import {TYPES, initialState, {{camelCase name}}Reducer} from './{{pascalCase name}}Reducer';
// import currencyFormatter from '@src/helpers/currencyFormatter';
function {{pascalCase name}}Container() {
    const [state, dispatch] = useReducer({{camelCase name}}Reducer, initialState);
   
    return <div className={style.container}>
        <h1>{{name}} Tester</h1>
        <div className={style.componentContainer}>
            <{{pascalCase name}}
--ADD-PROPS-HERE--
            />
        </div>
        <div className={style.controlContainer}>
            <div className={style.listenerDisplay} data-test="action-listener">
                {state.actionText}
            </div>
            {
                state.actionText && 
                <button data-test="reset-action-listener" onClick={() => {
                    dispatch({
                        type: TYPES.UPDATE,
                        payload: {
                            actionText: ''
                        }
                    });
                }}>reset action listener</button>
            }
--ADD-CONTROLS-HERE--            
        </div>
    </div>;
}

export default {{pascalCase name}}Container;