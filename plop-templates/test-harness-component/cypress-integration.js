const MENU = '{{pascalCase name}}';
describe('{{titleCase name}}', () => {

    beforeEach(() => {
        cy.visit('/');
        cy.selectDefaultFont();
        cy.getBySel('test-menu').clickLink(MENU);
    });

    it('should load the component', () => {
        cy.getClass('{{pascalCase name}}_container').should('exist');
    });
   
});

/**
 * modify your props here and check if the component
 * reacts with your props change
 */
describe('{{titleCase name}} - props', () => {

    before(() => {
        cy.visit('/');
        cy.selectDefaultFont();
        cy.getBySel('test-menu').clickLink(MENU);
    });

--PROPS_CHECK--
});


/**
 * Interact with your component here
 */
describe('{{titleCase name}} - interaction', () => {

    beforeEach(() => {
        cy.visit('/');
        cy.selectDefaultFont();
        cy.getBySel('test-menu').clickLink(MENU);
    });

//     it('should do things', () => {
//         cy.getBySel('trade-amount').clear().type(testValue);
//         cy.getBySel('trade-amount').should('have.value', testValue);
//         cy.getBySel('amount').should('have.value', testValue);
//         cy.getBySel('amount-string').should('have.value', testValue);
//     });
});
