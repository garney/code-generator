type stateType = {
--ADD-STATE-TYPE-HERE--
}

export const initialState:stateType = {
--ADD-DEFAULT-STATE-HERE--
};

export function {{camelCase name}}Reducer(state, action) {
    switch (action.type) {
        case TYPES.UPDATE:
            return {
                ...state,
                ...action.payload
            };
        default: 
            return {...state};
    }
}

export const TYPES = {
    UPDATE: 'UPDATE'
};