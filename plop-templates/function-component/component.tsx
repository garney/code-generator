import React from 'react';
import style from './{{pascalCase name}}.module.sass';
interface {{pascalCase name}}Type {
	message?: string;
}

function {{pascalCase name}}({ message = '{{pascalCase name}}' }: {{pascalCase name}}Type) {
  
  return <React.Fragment>
    <h1 className={style['{{kebabCase name}}']}>Hello {message}</h1>
  </React.Fragment>;
}

export default {{pascalCase name}};
