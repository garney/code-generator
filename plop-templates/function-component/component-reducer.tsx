import React, { useReducer } from 'react';
import style from './{{pascalCase name}}.module.sass';
interface {{pascalCase name}}Type {
	message?: string;
}

//#region reducer
interface State {
  greeting: string;
  value: number;
}

type Action =
  | { type: 'changeGreeting', payload: string }
  | { type: 'increment' }
  | { type: 'decrement' }
  | { type: 'incrementAmount'; payload: number };

const {{camelCase name}}Reducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'increment':
      return { ...state, value: state.value + 1 };
    case 'decrement':
      return { ...state, value: state.value - 1 };
    case 'incrementAmount':
      return { ...state, value: state.value + action.payload };
    case 'changeGreeting':
      return { ...state, greeting: action.payload };
    default:
      throw new Error();
  }
};
//#endregion

function {{pascalCase name}}({ message = '{{pascalCase name}}' }: {{pascalCase name}}Type) {
  const [state, dispatch] = useReducer({{camelCase name}}Reducer, { value: 0, greeting: 'Hello' });

  return <React.Fragment>
    <h1 className={style['{{kebabCase name}}']}>{state.greeting} {message} {state.value}</h1>
    <div>
      <button onClick={() => {dispatch({type: 'increment'})}}>increment</button>
      <button onClick={() => {dispatch({type: 'changeGreeting', payload: 'Hi'})}}>Change Greeting</button>
    </div>
  </React.Fragment>;
}

export default {{pascalCase name}};
