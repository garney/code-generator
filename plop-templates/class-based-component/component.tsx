import React from 'react';

import * as styles from './{{pascalCase name}}.module.sass';


interface {{pascalCase name}}Props {
    className?: string;
    id?: string;
}

interface {{pascalCase name}}State {
    enabled: boolean;
}

export default class {{pascalCase name}} extends React.Component<{{pascalCase name}}Props, {{pascalCase name}}State> {
    constructor(props: {{pascalCase name}}Props) {
        super(props);
    }

    state = {
        enabled: false,
    };

    componentDidMount = () => {
        
    };

    componentWillUnmount = () => {
        
    };

    render() {
        return (
        <div className="{{kebabCase name}}" id={this.props.id}>
             <h1>{{name}}</h1>
        </div>
       
        );
    }
}
